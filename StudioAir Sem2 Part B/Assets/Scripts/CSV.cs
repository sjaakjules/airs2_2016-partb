﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CSV : MonoBehaviour
{
    // Load you CSV in the inspector
    [Header("Load you CSV Here:")]
    public TextAsset[] DataMapCSV;


    [Header("CSV status")]
    public int dataValuesRead;
    public bool[] hasReadCSV;
    public int gridWidth;
    public int gridHeight;
    public float cellRadius;


    // These are variables which you access from other scripts.
    public static List<Vector3> dataMap;
    static List<float>[] data;
    public static float CellRadius;

    public static int nData { get { return data.Length; } }

   


    char row = '\n';
    char column = ',';


    // Use this for initialization
    void Awake()
    {

        if (Enum.GetNames(typeof(CSVmaps)).Length != DataMapCSV.Length)
        {
            Debug.LogError("You have a different number of CSV maps in your CSVManager from the types written in your code!");
        }
        // Setup the list of data and point lists to be the same size as the number of intensity maps loaded
        dataMap = new List<Vector3>();
        data = new List<float>[DataMapCSV.Length];
        hasReadCSV = new bool[DataMapCSV.Length];

        bool isSettingPositions = false;
        bool hasSetPositions = false;

        // For each intensity map we need to tell the computer we will have a list of points (vector3) and data values
        for (int j = 0; j < DataMapCSV.Length; j++)
        {
            data[j] = new List<float>();


            string[] lines = DataMapCSV[j].text.Split(row);
            string[] line;

            float tempx, tempy, tempz;


            if (lines[0].Split(column).Length == 4 && hasSetPositions == false)
            {
                isSettingPositions = true;
            }

            for (int i = 1; i < lines.Length; i++)
            {
                line = lines[i].Split(column);
                if (line.Length == 3 && float.TryParse(line[0], out tempx) && float.TryParse(line[1], out tempy) && float.TryParse(line[2], out tempz))
                {
                    if (isSettingPositions)
                    {
                        dataMap.Add(new Vector3(tempx, tempz, tempy));
                    }
                    data[j].Add(tempz);
                }
            }


            if (isSettingPositions)
            {
                line = lines[0].Split(column);
                if (line.Length == 4)
                {
                    if (float.TryParse(line[3], out cellRadius))
                    {
                        CellRadius = cellRadius;
                    }
                    if (int.TryParse(line[1], out gridWidth))
                    {
                        gridHeight = dataMap.Count / gridWidth;
                    }
                }
            }

            dataValuesRead = data[j].Count;

            if (data[j].Count > 0)
            {
                hasReadCSV[j] = true;
            }

            if (lines[0].Split(column).Length == 4 && isSettingPositions == true)
            {
                hasSetPositions = true;
                isSettingPositions = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static int FindIndex(Vector3 Position)
    {
        int index =  Array.FindIndex(dataMap.ToArray(), x => Math.Abs(x.x - Position.x) <= CellRadius &&
                                                                            Math.Abs(x.z - Position.z) <= CellRadius);
        return index;
    }

    public static float getIntensity(CSVmaps intensityMap, int GridIndex)
    {
        if (GridIndex >= 0 && GridIndex < data[(int)intensityMap].Count)
        {
            return data[(int)intensityMap][GridIndex];
        }
        return float.NaN;
    }

    /// <summary>
    /// This will add the new value to the intensity map at the GridIndex
    /// </summary>
    /// <param name="newValue"></param>
    /// <param name="intensityMap"></param>
    /// <param name="GridIndex"></param>
    public static void setIntensity(float newValue, CSVmaps intensityMap, int GridIndex)
    {
        if (GridIndex>=0 && GridIndex< data[(int)intensityMap].Count)
        {
            data[(int)intensityMap][GridIndex] += newValue;
        }
    }

    public static float[] getIntensities(int GridIndex)
    {
        if (GridIndex >= 0 && GridIndex < data[0].Count)
        {
            float[] intensityOut = new float[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                intensityOut[i] = data[i][GridIndex];
            }
            return intensityOut;
        }
        return null;
    }

}
