using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;


public class SpawnPickups : MonoBehaviour {

    public GameObject[] pickups;

    public int maxPickups = 10;
    public int spawnedPickups;

    public CSVmaps CSVSpawnPickupMap = 0;

    public static List<GameObject> SpawnedPickups = new List<GameObject>();
    public static List<int> SpawnedPickupIndicies = new List<int>();
    List<Vector3> intensityMap = new List<Vector3>();
    List<int> indicie;

    Vector3 location;
	public Color Pcol;
	MeshRenderer pickUpRender;

    


	// Use this for initialization
	void Start () 
	{
        intensityMap = CSV.dataMap;

        if (Enum.GetNames(typeof(PickupTypes)).Length != pickups.Length)
        {
            Debug.LogError("You have a different number of pickup types in your Pickup Manager from the types written in your code!");
        }

        // This will randomise the list so you can limit how many nodes you will generate.
        indicie = Enumerable.Range(0, CSV.dataMap.Count).ToList();
        indicie.Shuffle();
        
        // The For loop will cycle over the shuffled list of the data points within the loaded CSV intesity/data maps.
        for (int i = 0; i < intensityMap.Count; i++)
        {
            if (spawnedPickups >= maxPickups)
            {
                break;
            }
            else
            {
                Vector3 Position = intensityMap[indicie[i]];

                float spawnIntensity = CSV.getIntensity(CSVSpawnPickupMap,indicie[i]);


                ////////////////////////////////////////////////////////////////////////////////
                //                   Modify the if Statements here:
                //              This will set when different types will spawn.
                //      If you have an else at the end something will always be spawned.
                //          
                //       Modify inside an If statement to change what the types look like
                //
                //            Remember that the postion is x/z in unity with 
                //                  the height being the data value, y
                //          
                ////////////////////////////////////////////////////////////////////////////////
                if (spawnIntensity > 15 )                        // High intensity  
                {
                    // This is the characteristics of pickup type 1. I have loaded a unique GameObject (prefab draged in inspector)
                    // I have selected it to be a fixed colour with the size varying according to the intensity at this location.
                    // Not changing the height will leave them being populated with their intensities defining their height.
                    
                    int PickupNumber = (int)PickupTypes.Type1;

                    Position.y = 3;    // Changes the height, ie Make sure the pickup is 3 m high.
                    GameObject tempObj = Instantiate(pickups[PickupNumber], Position, Quaternion.identity) as GameObject;  // Creates a pickup at the location of the dataMap

                    tempObj.GetComponent<MeshRenderer>().material.color = new Color(0.6f, 0.4f, 0.2f);            // Changes the colour to some fixed value

                    tempObj.transform.localScale = new Vector3(spawnIntensity / 10, spawnIntensity / 10, spawnIntensity / 10);                         // Changes the size of the object according the the intensity

                    tempObj.transform.parent = gameObject.transform;                                            // used for the count score in the player options
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;                   
                }
                else if (spawnIntensity > 5 && spawnIntensity < 12)      // Mid intensity
                {
                    // This is the characteristics of a Standard pickup. 
                    // I have seelected it to be a fixed small size, grey colour and fixed height.

                    int PickupNumber = (int)PickupTypes.Type1;

                    Position.y = 2;    // Make sure the pickup is 3 m high.
                    GameObject tempObj = Instantiate(pickups[PickupNumber], Position, Quaternion.identity) as GameObject;      // Creates a pickup at the location of this point within the intensity/data map

                    tempObj.GetComponent<MeshRenderer>().material.color = Color.grey;                           // Changes the colour to some fixed value

                    tempObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);                                 // Changes the size of the object to be 0.1 of its normal size.

                    tempObj.transform.parent = gameObject.transform;
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;
                }
                else                                    // All other points
                {
                    // This is the characteristics of a bad pickup.
                    // I have selected the colour to vary according to the data, knowing 1.8 is the max

                    int PickupNumber = (int)PickupTypes.Type1;

                    Position.y = 2;    // Make sure the pickup is 3 m high.
                    GameObject tempObj = Instantiate(pickups[PickupNumber], Position, Quaternion.identity) as GameObject;           // Creates a pickup at the location of this point within the intensity/data map

                    tempObj.GetComponent<MeshRenderer>().material.color = new Color(0f, (spawnIntensity - 5) / 7, (spawnIntensity - 5) / 7);      // Changes the colour to be based on the data at this point

                    tempObj.transform.localScale = new Vector3(1, 1, 1);

                    tempObj.transform.parent = gameObject.transform;
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    
}



////////////////////////////////////////////////////////////////////////////////
//                   IGNORE!!!! DO NOT TOUCH BELOW
////////////////////////////////////////////////////////////////////////////////

public static class CustomFunctions
{
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

}
