﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour 
{

public AudioClip myMusic;
public AudioSource music;

    public float volume;
    public int mapNo;
    public int range;

    void Start()
    {
    music = GetComponent<AudioSource>();
    music.clip = myMusic;
        music.Play();
    }
    void Update()
    {
        volume = PlayerPickups._intensity[mapNo];
        music.volume = volume/range;
    }


}