﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum AgentType { Type1, Type2, Type3}

public enum CSVmaps { PickupSpawn, Score, AgentSpawn, Topography, AgentFood}

public enum PickupTypes { Type1, Type2, Type3}


public class AgentManager : MonoBehaviour {

    [Header("Load your Agents here:")]
    public GameObject[] Agent;
    public static int nAgents;
    
    public int maxNumberOfAgents = 10;

    public CSVmaps CSVSpawnAgentMap = CSVmaps.PickupSpawn;

    public bool spawnRandom = true;

    int spawnedAgents;
   

    // Use this for initialization
    void Start () {

        nAgents = Agent.Length;
        if (System.Enum.GetNames(typeof(AgentType)).Length != nAgents)
        {
            Debug.LogError("You have a different number of agents in your AgentManager from the types written in your code!");
        }

        if (spawnRandom)
        {
            for (int i = 0; i < maxNumberOfAgents; i++)
            {
                int newAgentType = Random.Range(0, nAgents);
                Vector3 position = CSV.dataMap[Random.Range(0, CSV.dataMap.Count)];

                GameObject tempObj = Instantiate(Agent[newAgentType], position, Quaternion.identity) as GameObject;  // Creates an agent at a random location of the dataMap

                tempObj.transform.parent = gameObject.transform;
            }
        }
        else
        {
            SpawnAgents();
        }


    }
	
	// Update is called once per frame
	void Update () {
        



    }


    void SpawnAgents()
    {
        List<Vector3> intensityMap = CSV.dataMap;

        // This will randomise the list so you can limit how many nodes you will generate.
        List<int> indicie = Enumerable.Range(0, CSV.dataMap.Count).ToList();
        indicie.Shuffle();

        // The For loop will cycle over the shuffled list of the data points within the loaded CSV intesity/data maps.
        for (int i = 0; i < intensityMap.Count; i++)
        {
            if (spawnedAgents >= maxNumberOfAgents)
            {
                break;
            }
            else
            {
                Vector3 Position = intensityMap[indicie[i]];

                float SawnIntensity = CSV.getIntensity(CSVSpawnAgentMap,indicie[i]);
                
                ////////////////////////////////////////////////////////////////////////////////
                //                   If Statements to spawn at different locations
                //              
                ////////////////////////////////////////////////////////////////////////////////
                if (SawnIntensity > 18)                        
                {
                    int agentTypeNumber = (int)AgentType.Type1;

                    GameObject tempObj = Instantiate(Agent[agentTypeNumber], Position, Quaternion.identity) as GameObject;      // Creates a pickup at the location of this point within the intensity/data map
                    tempObj.GetComponent<Agent>().agentType = (AgentType)agentTypeNumber;
                    tempObj.transform.parent = gameObject.transform;

                    spawnedAgents++;
                }
                else if (SawnIntensity > 1 && SawnIntensity < 4)
                {
                    int agentTypeNumber = (int)AgentType.Type2;

                    GameObject tempObj = Instantiate(Agent[agentTypeNumber], Position, Quaternion.identity) as GameObject;      // Creates a pickup at the location of this point within the intensity/data map
                    tempObj.GetComponent<Agent>().agentType = (AgentType)agentTypeNumber;
                    tempObj.transform.parent = gameObject.transform;

                    spawnedAgents++;
                }
                else
                {
                    //GameObject tempObj = Instantiate(Agent[2], Position, Quaternion.identity) as GameObject;      // Creates a pickup at the location of this point within the intensity/data map
                    //tempObj.transform.parent = gameObject.transform;

                    //spawnedAgents++;
                }
            }
        }
    }
    
}
