﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    Rigidbody rb;
    public Text countText;
    public Text winText;
    public GameObject pickupController;

    public float speed = 10;

    int count = 0;



	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        countText.text = "Count: 0";
        winText.text = "";
	}
	
	// Update is called once per frame, if on a slow phone will randomly drop frames
	void Update () {
	    
	}

    // This is a fixed update and is used for stable maths calculations.
    void FixedUpdate()
    {
        float movehorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(movehorizontal, 0.0f,moveVertical);
        rb.AddForce(movement * speed);

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            setCountText();
        }
    }

    void setCountText()
    {
        count++;
        countText.text = "Count: " + count.ToString();
        if (count >= pickupController.transform.childCount)
        {
            winText.text = "You have won,\nWhat an achievement.....";
        }
    }
}
