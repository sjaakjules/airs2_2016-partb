﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System;

public class PlayerPickups : MonoBehaviour {

    public Text countText;
    public Text winText;
    public GameObject pickupController;

    public static float[] _intensity;

    public CSVmaps CSVScoreMap = 0;

    int count = 0;
    int type1Count = 0;
    int winPickupCount;

    float score = 0;
    public float winScore = 100;
    public float looseScore = -100;



    // Use this for initialization
    void Start()
    {
        _intensity = new float[CSV.nData];
        countText.text = "Count: 0";
        winText.text = "";
    }

    // Update is called once per frame, if on a slow phone will randomly drop frames
    void Update()
    {
        int gridIndicie = CSV.FindIndex(transform.position);
        _intensity = CSV.getIntensities(gridIndicie);


        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //                                            Display text
        //                          1   Update the scores and count
        //                          2   If winconditions are met, display the win text or loose
        //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        countText.text = "Count: " + count.ToString() + "\nScore: " + score.ToString("F0");

        if (winPickupCount ==4)
        {
            winText.text = "You have won,\nWhat an achievement.....";
        }

        if (count >= pickupController.transform.childCount || score >= winScore)
        {
            winText.text = "You have won,\nWhat an achievement.....";
        }
        else if (score < looseScore)
        {
            winText.text = "You have lost,\nPerhaps try something else.....";
        }
    }

    // This is a fixed update and is used for stable maths calculations.
    void FixedUpdate()
    {

    }


    void OnTriggerEnter(Collider other)
    {
        // This will get the position of the Collided pickup and find where in the list (intensityMapIndex) is the closest grid
        int intensityMapIndex = CSV.FindIndex(other.gameObject.transform.position);

        // Now you can get the data value which is closest to this pickup.
        float[] intensityOfPickup = CSV.getIntensities(intensityMapIndex);
        

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //                                  For each type of pickup
        //
        //                         1         Count the pickups collied
        //                         2     change the score of the player
        //                         3     turn the collided pickups off
        //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        if (other.gameObject.CompareTag("PickupType1"))
        {
            // Count the number of pickups
            count++;
            type1Count++;

            // Change the player score according to the intensity map
            score -= intensityOfPickup[(int)CSVmaps.Score];

            // Turn off the collided pickup
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("PickupType2"))
        {
            // Count the number of pickups
            count++;

            // Change the player score according to the intensity map
            score -= intensityOfPickup[(int)CSVmaps.Score];

            // Turn off the collided pickup
            other.gameObject.SetActive(false);
            
        }
        else if (other.gameObject.CompareTag("PickupType3"))
        {
            // Count the number of pickups
            count++;

            // Change the player score according to the intensity map
            score += intensityOfPickup[(int)CSVmaps.Score];
            
            // Turn off the collided pickup
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Pick Up"))
        {
            // Count the number of pickups
            count++;

            winPickupCount++;

            // Change the player score according to the intensity map
            score += intensityOfPickup[(int)CSVmaps.Score];

            // Turn off the collided pickup
            other.gameObject.SetActive(false);
        }
    }
    
    
}
